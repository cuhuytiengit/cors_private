var express = require("express");
const cors = require("cors");
var jobRouter = express.Router();
const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Request-Method", "GET");
  res.setHeader("Access-Control-Allow-Private-Network", "true");
  next();
});
app.use(express.json());
app.use("/api", jobRouter);
// app.use((req, res, next) => {
//   res.setHeader("Access-Control-Allow-Origin", "*");
//   res.setHeader("Access-Control-Allow-Private-Network", "true");
//   next();
//   res.setHeader(
//     "Access-Control-Allow-Methods",
//     "GET, POST, OPTIONS, PUT, PATCH, DELETE"
//   ); // If needed
//   response.setHeader("Access-Control-Allow-Private-Network", true);
//   res.writeHead(200, {
//     "Access-Control-Allow-Headers": true,
//   }); // If needed
//   res.setHeader("Access-Control-Allow-Credentials", true);
//   next();
// });

app.listen(3000, function () {
  console.log("Connected Successfull!");
});
